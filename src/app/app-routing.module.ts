import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MenuLazyComponent } from './menu-lazy/menu-lazy.component';
import { ItemAComponent } from './menu/item-a/item-a.component';
import { ItemBComponent } from './menu/item-b/item-b.component';
import { ItemCComponent } from './menu/item-c/item-c.component';
import { MenuComponent } from './menu/menu.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouteParamsComponent } from './route-params/route-params.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  /**
   * Carregando componentes no bootstrap da aplicação
   */
  {
    path: 'menu',
    component: MenuComponent,
    children: [
      ///
      { path: 'itemA', component: ItemAComponent },
      { path: 'itemB', component: ItemBComponent },
      { path: 'itemC', component: ItemCComponent },
      { path: '**', component: NotFoundComponent },
    ],
  },
  /**
   * Modulo em carregamento lento
   */
  {
    path: 'menulazyload',
    component: MenuLazyComponent,
    loadChildren: () =>
      import('./menu-lazy/menu-lazy.module').then((m) => m.MenuLazyModule),
  },
  /**
   * Route params, modulo em carregamento lento
   */
  {
    path: 'route-params',
    component: RouteParamsComponent,
    loadChildren: () =>
      import('./route-params/route-params.module').then(
        (m) => m.RouteParamsModule
      ),
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
