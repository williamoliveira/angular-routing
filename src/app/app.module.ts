import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { MenuModule } from './menu/menu.module';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { MenuLazyModule } from './menu-lazy/menu-lazy.module';
import { RouteParamsModule } from './route-params/route-params.module';

@NgModule({
  declarations: [
    ///
    AppComponent,
    NotFoundComponent,
    HomeComponent,
  ],
  imports: [
    ///
    BrowserModule,
    MenuModule,
    RouterModule,
    AppRoutingModule,
    MenuLazyModule,
    RouteParamsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
