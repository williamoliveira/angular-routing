import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BranchAModule } from './branch-a/branch-a.module';
import { RouteBComponent } from './route-b.component';

@NgModule({
  declarations: [RouteBComponent],
  exports: [RouteBComponent],
  imports: [
    ///
    BranchAModule,
    CommonModule,
  ],
})
export class RouteBModule {}
