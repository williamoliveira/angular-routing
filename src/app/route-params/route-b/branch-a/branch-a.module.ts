import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BranchAComponent } from './branch-a.component';

@NgModule({
  declarations: [BranchAComponent],
  exports: [BranchAComponent],
  imports: [CommonModule],
})
export class BranchAModule {}
