import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BranchBRoutingModule } from './branch-b-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BranchBRoutingModule
  ]
})
export class BranchBModule { }
