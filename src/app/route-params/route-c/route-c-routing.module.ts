import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchBComponent } from './branch-b/branch-b.component';
import { BranchCComponent } from './branch-c/branch-c.component';

const routes: Routes = [
  { path: 'branchB', component: BranchBComponent },
  { path: 'branchC', component: BranchCComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteCRoutingModule {}
