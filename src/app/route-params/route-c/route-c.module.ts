import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteCRoutingModule } from './route-c-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouteCRoutingModule
  ]
})
export class RouteCModule { }
