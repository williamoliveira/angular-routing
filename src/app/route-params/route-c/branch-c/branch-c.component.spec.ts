import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchCComponent } from './branch-c.component';

describe('BranchCComponent', () => {
  let component: BranchCComponent;
  let fixture: ComponentFixture<BranchCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BranchCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
