import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BranchCRoutingModule } from './branch-c-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BranchCRoutingModule
  ]
})
export class BranchCModule { }
