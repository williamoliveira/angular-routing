import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteARoutingModule } from './route-a-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouteARoutingModule
  ]
})
export class RouteAModule { }
