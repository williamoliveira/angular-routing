import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteAComponent } from './route-a/route-a.component';
import { BranchAComponent } from './route-b/branch-a/branch-a.component';
import { RouteBComponent } from './route-b/route-b.component';
import { RouteCComponent } from './route-c/route-c.component';

const routes: Routes = [
  ///
  { path: 'routeA', component: RouteAComponent },
  { path: 'routeB', component: RouteBComponent },
  { path: 'branchA', component: BranchAComponent },
  /**
   * Modulo em carregamento lento
   */
  {
    path: 'routeC',
    component: RouteCComponent,
    loadChildren: () =>
      import('./route-c/route-c.module').then((m) => m.RouteCModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteParamsRoutingModule {}
