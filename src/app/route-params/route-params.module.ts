import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteParamsRoutingModule } from './route-params-routing.module';
import { RouteCComponent } from './route-c/route-c.component';
// import { RouteBComponent } from './route-b/route-b.component';
import { RouteAComponent } from './route-a/route-a.component';
import { RouteParamsComponent } from './route-params.component';
import { RouteBModule } from './route-b/route-b.module';

@NgModule({
  declarations: [
    ///
    RouteAComponent,
    // RouteBComponent,
    RouteCComponent,
    RouteParamsComponent,
  ],
  imports: [
    ///
    CommonModule,
    RouteParamsRoutingModule,

    RouteBModule,
  ],
})
export class RouteParamsModule {}
