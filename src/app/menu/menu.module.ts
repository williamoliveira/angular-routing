import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuRoutingModule } from './menu-routing.module';
import { ItemCComponent } from './item-c/item-c.component';
import { ItemBComponent } from './item-b/item-b.component';
import { ItemAComponent } from './item-a/item-a.component';
import { MenuComponent } from './menu.component';

@NgModule({
  declarations: [
    ///
    ItemCComponent,
    ItemBComponent,
    ItemAComponent,
    MenuComponent,
  ],
  imports: [CommonModule, MenuRoutingModule],
})
export class MenuModule {}
