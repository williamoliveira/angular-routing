import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemARoutingModule } from './item-a-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ItemARoutingModule
  ]
})
export class ItemAModule { }
