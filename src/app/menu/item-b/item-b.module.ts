import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemBRoutingModule } from './item-b-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ItemBRoutingModule
  ]
})
export class ItemBModule { }
