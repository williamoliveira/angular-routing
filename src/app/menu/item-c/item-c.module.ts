import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemCRoutingModule } from './item-c-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ItemCRoutingModule
  ]
})
export class ItemCModule { }
