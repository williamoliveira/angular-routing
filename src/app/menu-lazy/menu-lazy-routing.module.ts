import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from '../not-found/not-found.component';

import { ItemYComponent } from './item-y/item-y.component';
import { ItemZComponent } from './item-z/item-z.component';

const routes: Routes = [
  /**
   * Este item está sendo carregado em Lazy Load
   */
  {
    path: 'itemX',
    loadChildren: () =>
      import('./item-x/item-x.module').then((m) => m.ItemXModule),
  },
  /**
   * Componentes sendo carregados diretamente
   */
  { path: 'itemY', component: ItemYComponent },
  { path: 'itemZ', component: ItemZComponent },
  /**
   * Tratativa de erro / Excessão
   */
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuLazyRoutingModule {}
