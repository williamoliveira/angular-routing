import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemZComponent } from './item-z.component';

describe('ItemZComponent', () => {
  let component: ItemZComponent;
  let fixture: ComponentFixture<ItemZComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemZComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemZComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
