import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemZComponent } from './item-z.component';

@NgModule({
  declarations: [ItemZComponent],
  imports: [CommonModule],
})
export class ItemZModule {}
