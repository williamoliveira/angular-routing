import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuLazyRoutingModule } from './menu-lazy-routing.module';
import { MenuLazyComponent } from './menu-lazy.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [MenuLazyComponent],
  imports: [CommonModule, MenuLazyRoutingModule],
})
export class MenuLazyModule {}
