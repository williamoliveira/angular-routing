import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemXComponent } from './item-x.component';

const routes: Routes = [{ path: '', component: ItemXComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ItemXRoutingModule {}
