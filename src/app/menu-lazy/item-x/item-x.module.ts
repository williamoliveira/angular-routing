import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemXRoutingModule } from './item-x-routing.module';
import { ItemXComponent } from './item-x.component';


@NgModule({
  declarations: [ItemXComponent],
  imports: [
    CommonModule,
    ItemXRoutingModule
  ]
})
export class ItemXModule { }
