import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemYComponent } from './item-y.component';

@NgModule({
  declarations: [ItemYComponent],
  imports: [CommonModule],
})
export class ItemYModule {}
